#===============================================================================
#USAGE : ./ms1-menu.sh orientation
#DESCRIPTION : Opens a terminal menu to execute restart, shutdown, turns-creen, keyboard-language scripts.
#PARAMS: 
#TIPS:
#===============================================================================
#
#!/bin/bash
#START

echo Choose your operation:

echo "(1) Shutdown"
echo "(2) Reboot"
echo "(3) Turn screen"
echo "(4) Change keyboard language"

read input

case "$input" in
	1) echo You have chosen to shutdown the computer
		sleep 1;
		bash shutdown.sh
		;;

	2) echo You have chosen to reboot the computer
		sleep 1;
		bash restart.sh
		;;

	3) echo You have chosen to turn your screen
		echo "Please, chose the orientation (right | left)"
		read orientation_input
		echo Rotating to the "$orientation_input"
		bash turn-screen.sh "$orientation_input"
		;;

	4) echo You have chosen to change your keyboard lanague
		echo "Please, insert the desired language (us | br | de ...)"
		read language_input
		echo Changing to "$language_input"
		bash keyboard-language.sh "$language_input"

esac

#END
#
# Copyright Liquid Galaxy ORG.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
